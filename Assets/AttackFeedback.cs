using UnityEngine;

public enum AttackFeedbackReaction { CUTS_THRU, KNOCKS_BACK }

public class AttackFeedback
{
    public Vector3 myLocation;
    public AttackFeedbackReaction reaction;
    public float bounciness;

    public AttackFeedback(Vector3 myLocation, AttackFeedbackReaction reaction, float bounciness)
    {
        this.myLocation = myLocation;
        this.reaction = reaction;
        this.bounciness = bounciness;
    }
}