﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    public float speed = 200;
    public float jumpSpeed = 35;                private bool _requestJump, _waitUntilNewAttackKeyPress;
    public float groundPoundForce = 150;
    public float maxMagnitude = 17;             private float _realMaxMagnitude;                private bool _requestIncreaseMagnitude;
    public float maxMagnitudeSprint = 25;
    public float defaultFriction = 3;           private string currentGround;
    public int maxMidairJumps = 1;                    private int _numJumps;
    public RectTransform staminaBarUIMain, staminaBarUIRecover;
    public float staminaMax = 15;               private float _currentStamina;
    public float staminaRecoverTime = 1f;       private float _staminaRecoverDebounce;
    public float attackStaminaDecrease = 10;
    public float attackHoldStaminaDecrease = 10;
    public float sprintStaminaDecrease = 3;
    public float helicopterSpinVerticalSpeed = 2.25f;
    public float jumpLeewayFalling = 0.25f;     private float _jumpLeewayFallingTimer = 0;
    public float jumpLeewayInput = 0.25f;       public float _jumpLeewayInputTimer = 1000;
    public Camera refCamera;
    public SliceHandler sliceHandler;
    private Rigidbody myRigidbody;
    private Vector3 outputMovement;
    private Vector3 facingDirection;
    private Vector3 myPrevVelo = Vector3.zero;
    private bool onGround;
    private Renderer meshRenderer;
    private float disableInputTime;

    void Start()
    {
        // Starts the application code stuff
        Application.targetFrameRate = 60;
        Cursor.lockState = CursorLockMode.Locked;

        myRigidbody = GetComponent<Rigidbody>();
        refCamera = refCamera == null ? Camera.main.GetComponent<Camera>() : refCamera;
        sliceHandler = sliceHandler == null ? GetComponentInChildren<SliceHandler>() : sliceHandler;
        meshRenderer = GetComponent<Renderer>();
        DecreaseStamina(_currentStamina - staminaMax);
        _numJumps = 0;
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            _requestJump = true;
            _jumpLeewayInputTimer = 0;
        }
        if (Input.GetButtonDown("ToggleCheatMode"))
        {
            // DISABLE ME AND ENABLE GHOST MODE
            myRigidbody.isKinematic = true;
            GetComponent<PlayerGhostMode>().enabled = true;
            GetComponent<MeshCollider>().enabled = false;
            this.enabled = false;
        }
    }

    void FixedUpdate()
    {
        {
            RaycastHit checkLayer;
            int layer = -1;
            currentGround = string.Empty;
            onGround = Physics.Raycast(transform.position + new Vector3(0, -0.8f, 0), Vector3.down, out checkLayer, 0.75f);
            if (onGround)   // Real onGround (so checkLayer exists!)
            {
                currentGround = checkLayer.collider.material.name;
            }
            else
            {
                // SUPPLEMENTARY check if bounced too fast!
                if (myPrevVelo.y < 0 && myRigidbody.velocity.y >= -myPrevVelo.y)
                {
                    onGround = true;
                    layer = LayerMask.NameToLayer("Sticky");    // Assume that it's a sticky trampoline surface
                }
            }

            meshRenderer.material.SetColor("_Color", onGround ? Color.gray : Color.red);
            if (onGround)   // Synthesized onGround
            {
                layer = layer < 0 ? checkLayer.collider.gameObject.layer : layer;
                if (layer == LayerMask.NameToLayer("Sticky"))
                {
                    Vector3 red = myRigidbody.velocity;
                    red.x = red.z = 0;
                    myRigidbody.velocity = red;
                    DisableInputTemporarily();

                    // Probably bouncy so add the stamina back!!
                    _staminaRecoverDebounce = 0;
                    DecreaseStamina(0);
                }
                else if (layer == LayerMask.NameToLayer("Slippery"))
                {
                    if (_realMaxMagnitude == maxMagnitude)
                    {
                        ExpandMaxMagnitudeTemporarily();
                    }
                }
            }
        }

        // RESET ONGROUND
        if (onGround)
        {
            if (_staminaRecoverDebounce <= 0)
            {
                DecreaseStamina(_currentStamina - staminaMax);
            }
            else
            {
                _staminaRecoverDebounce -= Time.deltaTime;
                DecreaseStamina(0);
                if (_currentStamina <= 0)
                    DisableInputTemporarily();      // So that player can't move until stamina is recovered
            }
            _jumpLeewayFallingTimer = 0;
            _numJumps = 0;
        }
        else
        {
            _jumpLeewayFallingTimer += Time.deltaTime;
        }

        // INPUT
        float mag;
        Vector3 deltaMvt =
            JustYRotation(refCamera.transform.rotation)
                * GetInputMovement(out mag);
        myRigidbody.AddForce(deltaMvt * speed * Time.deltaTime, ForceMode.VelocityChange);

        // If deltaMvt has a large enough magnitude, then it should be made as a new facingDirection
        if (deltaMvt.magnitude > 0.05f)     // FOR PERFORMANCE use sqrMagnitude instead
        {
            facingDirection = deltaMvt;
        }

        LimitRigidbodyMagnitude(mag);

        // JUMP!!!!
        if (_requestJump)
        {
            if (_jumpLeewayInputTimer > jumpLeewayInput)
            {
                _requestJump = false;       // Reset after input window ends
            }
            _jumpLeewayInputTimer += Time.deltaTime;

            bool _adjustedOnGround = onGround || _jumpLeewayFallingTimer <= jumpLeewayFalling;
            if (_adjustedOnGround || _numJumps < maxMidairJumps)
            {
                if (!_adjustedOnGround) _numJumps++;
                if (sliceHandler.IsSlicing()) {
                    sliceHandler.StopSlice();
                    _waitUntilNewAttackKeyPress = true;
                }
                myRigidbody.velocity = Vector3.zero;
                myRigidbody.AddForce(
                    transform.up * jumpSpeed,
                    ForceMode.VelocityChange
                );

                _requestJump = false;       // Reset on success
            }
        }

        if (sliceHandler.IsSlicing())
        {
            DecreaseStamina(attackHoldStaminaDecrease * Time.deltaTime);
        }

        // ATTACK!!!
        if (Input.GetButton("Attack") && _currentStamina > 0)
        {
            if (!sliceHandler.IsSlicing() && !_waitUntilNewAttackKeyPress)
            {
                DecreaseStamina(attackStaminaDecrease);
                SwingDirection sd = SwingDirection.HORIZONTAL;
                sliceHandler.PostSlice(sd);
            }
        }
        else if (Input.GetButton("Attack2") && _currentStamina > 0)
        {
            if (!sliceHandler.IsSlicing() && !_waitUntilNewAttackKeyPress)
            {
                DecreaseStamina(attackStaminaDecrease);
                myRigidbody.velocity = Vector3.zero;    // For changing sharply that the vertical slice happened (only if in midair pls, bc onGround version's feel should not feel like a ground pound...)
                SwingDirection sd = SwingDirection.VERTICAL;
                sliceHandler.PostSlice(sd, facingDirection);
            }
        }
        // Cancel an attack anytime
        else 
        {
            _waitUntilNewAttackKeyPress = false;
            if (sliceHandler.IsSlicing())
                sliceHandler.StopSlice();
        }

        if (sliceHandler.GetSwingDirection() == SwingDirection.HORIZONTAL) {
            Vector3 myVelo = myRigidbody.velocity;
            myVelo.y = Mathf.Max(myVelo.y, helicopterSpinVerticalSpeed);
            myRigidbody.velocity = myVelo;
        }

        if (!onGround)   // If Midair eh!
        {
            if (sliceHandler.GetSwingDirection() == SwingDirection.VERTICAL) {
                Vector3 myVelo = myRigidbody.velocity;
                myVelo.y -= groundPoundForce * Time.deltaTime;
                myRigidbody.velocity = myVelo;
            }
        }

        myPrevVelo = myRigidbody.velocity;
    }

    public Vector3 GetInputMovement(out float magnitude)
    {
        // if (!sliceHandler.IsSlicing())
        {
            float hAxis = 0;
            float vAxis = 0;
            if (disableInputTime <= 0)
            {
                hAxis = Input.GetAxis("Horizontal");
                vAxis = Input.GetAxis("Vertical");
            }
            else
            {
                disableInputTime -= Time.deltaTime;
            }

            Vector3 hMove = hAxis * transform.right;
            Vector3 vMove = vAxis * transform.forward;

            float mag = Mathf.Min((hMove + vMove).magnitude, 1);

            outputMovement = (hMove + vMove).normalized * mag;
            magnitude = mag;        // Return!
        }
        return outputMovement;
    }

    void ExpandMaxMagnitudeTemporarily()        // Received by knockback action via message!
    {
        _requestIncreaseMagnitude = true;
    }

    private void LimitRigidbodyMagnitude(float inputMagnitude)      // Param inputMagnitude must be from 0 to 1
    {
        float tempMaxMagnitude = (Input.GetAxis("LeftTrigger") < 0 && _currentStamina > 0) ? maxMagnitudeSprint : maxMagnitude;

        Vector3 justXZ = myRigidbody.velocity;
        justXZ.y = 0;

        if (_requestIncreaseMagnitude)
        {
            _realMaxMagnitude = justXZ.magnitude;
            if (_realMaxMagnitude > tempMaxMagnitude)
            {
                _requestIncreaseMagnitude = false;
            }
        }

        if (_realMaxMagnitude > tempMaxMagnitude && !onGround)      // This way so that in midair you can have free game but on the ground kanarazu friction
        {
            // Update real max magnitude
            _realMaxMagnitude = Mathf.Max(Mathf.Min(_realMaxMagnitude, justXZ.magnitude), tempMaxMagnitude);        // If magnitude is over max magnitude, lower max until below basic maxMag
        }
        else
        {
            // Use friction data from other materials etc. (TODO)
            float magnitudeFromInput = inputMagnitude * tempMaxMagnitude;
            if (magnitudeFromInput >= _realMaxMagnitude)
            {
                // Set it so that limit is lifted
                _realMaxMagnitude = magnitudeFromInput;
            }
            else
            {
                // Slowly deaden the max magnitude from provided friction functions (under-cap at the requested magnitude from input)
                _realMaxMagnitude = Mathf.Max(_realMaxMagnitude - (GetCurrentFriction() * Time.deltaTime), magnitudeFromInput);
            }
        }

        // Limit magnitude
        justXZ = Vector3.ClampMagnitude(justXZ, _realMaxMagnitude);

        // Use stamina
        if (tempMaxMagnitude == maxMagnitudeSprint &&
            justXZ.magnitude > maxMagnitude)
        {
            DecreaseStamina(sprintStaminaDecrease * Time.deltaTime);
        }

        // Apply!
        justXZ.y = myRigidbody.velocity.y;
        myRigidbody.velocity = justXZ;
    }

    private float GetCurrentFriction()
    {
        if (currentGround.ToLower().StartsWith("ground"))
        {
            return 100;
        }
        else if (currentGround.ToLower().StartsWith("slippery"))
        {
            return 0;
        }
        return defaultFriction;
    }

    public Quaternion JustYRotation(Quaternion q)
    {
        Vector3 v3 = q.eulerAngles;
        v3.x = v3.z = 0;
        return Quaternion.Euler(v3);
    }

    public AttackFeedback PostAttack(AttackData attackData)
    {
        UnityHelperUtils.CreateKnockback(
            myRigidbody,
            transform.position,
            attackData.myLocation,
            attackData.bounciness
        );

        return null;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Enemy")
        {
            AttackFeedback af = other.GetComponent<EnemySlime>()
                .PostAttack(
                    new AttackData(transform.position, sliceHandler.GetSwingDirection(), 1, sliceHandler.sliceBounciness, false)
                );

            if (af != null)
            {
                UnityHelperUtils.CreateKnockback(myRigidbody, transform.position, af.myLocation, af.bounciness);
                DisableInputTemporarily();
            }
        }
        else if (other.tag == "Bouncy")
        {
            AttackFeedback af = other.GetComponent<BouncyObject>()
                .PostAttack(
                    new AttackData(transform.position, sliceHandler.GetSwingDirection(), 1, sliceHandler.sliceBounciness, false)
                );
            
            if (af != null)
            {
                UnityHelperUtils.CreateKnockback(myRigidbody, transform.position, af.myLocation, af.bounciness, false);
                DisableInputTemporarily();
            }
        }
    }

    private void DisableInputTemporarily(float time=0.15f)
    {
        disableInputTime = time;
    }

    private void DecreaseStamina(float amount)
    {
        _currentStamina -= amount;

        if (amount > 0)
            _staminaRecoverDebounce = staminaRecoverTime;

        // textReportObj.text = "Stamina: " + Mathf.Max(0, Mathf.Round(_currentStamina * 100f) / 100f);
        UpdateUIForStamina(
            Mathf.Max(0, _currentStamina),
            staminaMax,
            staminaRecoverTime - _staminaRecoverDebounce,
            staminaRecoverTime
        );
    }

    private void UpdateUIForStamina(
        float amountStamina,
        float totalStamina,
        float amountRecover,
        float totalRecover)
    {
        float maxWidth = 100f;

        // Main bar
        {
            float w = Mathf.Min(1f, amountStamina / totalStamina) * maxWidth;
            staminaBarUIMain.sizeDelta = new Vector2(w, staminaBarUIMain.sizeDelta.y);
        }

        // Recover bar
        {
            float w = Mathf.Min(1f, amountRecover / totalRecover) * maxWidth;
            staminaBarUIRecover.sizeDelta = new Vector2(w, staminaBarUIRecover.sizeDelta.y);
        }
    }
}
