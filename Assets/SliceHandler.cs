﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceHandler : MonoBehaviour
{
    public float sliceBounciness = 1;
    private bool slicing = false;
    private SwingDirection currentSwingDir;

    void Update()
    {
        if (!slicing)
        {
            gameObject.SetActive(false);
        }
    }

    public void PostSlice(SwingDirection swingDirection, Vector3 facingDirection=new Vector3())
    {
        // TODO: Fudge the input later!
        if (slicing) return;

        currentSwingDir = swingDirection;

        float angle = Mathf.Atan2(facingDirection.z, facingDirection.x) * Mathf.Rad2Deg;
        transform.rotation =
            Quaternion.Euler(
                currentSwingDir == SwingDirection.VERTICAL ? 90 : 0,
                0,
                currentSwingDir == SwingDirection.VERTICAL ? angle : 0
            );

        StartSlice();
    }

    public bool IsSlicing()
    {
        return slicing;
    }

    public SwingDirection GetSwingDirection()
    {
        if (!slicing) return SwingDirection.NONE;
        return currentSwingDir;
    }

    private void StartSlice()
    {
        slicing = true;
        gameObject.SetActive(true);
    }

    public void StopSlice()     // See if can refactor into the Update() method if wanted....
    {
        slicing = false;
        gameObject.SetActive(false);
    }
}
