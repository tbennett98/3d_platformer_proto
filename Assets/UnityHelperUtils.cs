using UnityEngine;

public class UnityHelperUtils
{
    public static Vector3 Vector3DifferenceIgnoreY(Vector3 a, Vector3 b, bool ignoreY=true)
    {
        if (ignoreY) a.y = b.y = 0;
        return a - b;
    }

    public static void CreateKnockback(
        Rigidbody myRigidbody,
        Vector3 myLoc,
        Vector3 otherLoc,
        float bounciness,
        bool ignoreYAxis=true)
    {
        Vector3 knockBackDirection =
            Vector3DifferenceIgnoreY(myLoc, otherLoc, ignoreYAxis).normalized;

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.AddForce(
            knockBackDirection * bounciness,
            ForceMode.VelocityChange
        );
        myRigidbody.SendMessage("ExpandMaxMagnitudeTemporarily");
    }
}