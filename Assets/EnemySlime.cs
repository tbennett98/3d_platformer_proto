﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySlime : MonoBehaviour, IEnemy
{
    public float bounciness;
    public AudioClip cutSound;
    public float cutWaitTime;
    public ParticleSystem psPreExplode;
    public ParticleSystem psExplode;
    public float explodeCountdown;
    public bool useAI = false;
    public Transform player;
    public float approachDist = 3;
    public float approachSpeed = 200;
    public float approachMaxMagnitude = 10;
    public float attackDist = 2;
    public float attackSpeed = 300;
    public float attackMaxMagnitude = 30;
    public float attackPauseTime = 0.5f;        private float attackTimeElapsed;
    public float attackSaveDirectionTime = 0.25f;      private Vector3 attackDir = Vector3.zero;
    public float attackTotalTime = 1f;
    public float attackLandedBounciness = 50f;
    public float attackAftermathTime = 0.25f;
    private Rigidbody myRigidbody;
    private SwingDirection cutDirection = SwingDirection.NONE;
    private SlimeStage currentStage = SlimeStage.IDLE;
    private enum SlimeStage { IDLE, APPROACH, ATTACK, ATTACK_AFTERMATH }

    void Start()
    {
        psPreExplode.gameObject.SetActive(false);
        psExplode.gameObject.SetActive(false);
        myRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
    	if (useAI)
    	{
            float dist = Vector3.Distance(player.position, transform.position);
            if (currentStage == SlimeStage.IDLE)
            {
                attackDir = Vector3.zero;       // Reset here
                if (dist <= approachDist)
                {
                    currentStage = SlimeStage.APPROACH;
                }
                myRigidbody.velocity = new Vector3(0, myRigidbody.velocity.y, 0);
            }
            else if (currentStage == SlimeStage.APPROACH)
            {
                if (dist > approachDist)
                {
                    currentStage = SlimeStage.IDLE;
                }
                else if (dist <= attackDist)
                {
                    currentStage = SlimeStage.ATTACK;
                    attackTimeElapsed = 0;
                }
                myRigidbody.AddForce(
                    (player.position - transform.position).normalized * approachSpeed * Time.deltaTime,
                    ForceMode.VelocityChange
                );
                Vector3 temp = myRigidbody.velocity;
                temp.y = 0;
                temp = Vector3.ClampMagnitude(temp, approachMaxMagnitude);
                temp.y = myRigidbody.velocity.y;
                myRigidbody.velocity = temp;
            }
            else if (currentStage == SlimeStage.ATTACK)
            {
                attackTimeElapsed += Time.deltaTime;
                if (attackDir == Vector3.zero)
                {
                    if (attackTimeElapsed >= attackSaveDirectionTime)
                    {
                        Vector3 vecDirToPlayer = player.position - transform.position;
                        vecDirToPlayer.y = 0;
                        attackDir = vecDirToPlayer.normalized;
                    }
                }

                if (attackTimeElapsed < attackPauseTime)
                {
                    myRigidbody.velocity = new Vector3(0, myRigidbody.velocity.y, 0);
                }
                else if (attackTimeElapsed < attackTotalTime)
                {
                    myRigidbody.AddForce(
                        attackDir * attackSpeed * Time.deltaTime,
                        ForceMode.VelocityChange
                    );
                    Vector3 temp = myRigidbody.velocity;
                    temp.y = 0;
                    temp = Vector3.ClampMagnitude(temp, attackMaxMagnitude);
                    temp.y = myRigidbody.velocity.y;
                    myRigidbody.velocity = temp;
                }
                else
                {
                    currentStage = SlimeStage.IDLE;
                }
            }
            else if (currentStage == SlimeStage.ATTACK_AFTERMATH)
            {
                attackTimeElapsed += Time.deltaTime;
                if (attackTimeElapsed >= attackAftermathTime)
                {
                    currentStage = SlimeStage.IDLE;
                }
            }
    	}

        if (cutDirection != SwingDirection.NONE &&
            !psExplode.gameObject.activeSelf)
        {

            explodeCountdown -= Time.deltaTime;

            if (explodeCountdown <= 0)
            {
                psPreExplode.Stop();
                psExplode.gameObject.SetActive(true);
                explodeCountdown = 0;
                DieHideSelf();
            }
        }
    }

    public AttackFeedback PostAttack(AttackData attackData)
    {
        if (cutSound != null)
        {
            AudioSource.PlayClipAtPoint(cutSound, transform.position);
        }

        if (cutDirection == SwingDirection.NONE)
        {
            UnityHelperUtils.CreateKnockback(
                GetComponent<Rigidbody>(),
                transform.position,
                attackData.myLocation,
                attackData.bounciness
            );
            cutDirection = attackData.slashDirection;

            // TODO: extract into seperate method
            Vector3 facingDirection = attackData.myLocation - transform.position;
            float angle = Mathf.Atan2(facingDirection.z, facingDirection.x) * Mathf.Rad2Deg;
            psPreExplode.gameObject.SetActive(true);
            psPreExplode.transform.rotation =
                Quaternion.Euler(
                    cutDirection == SwingDirection.VERTICAL ? 90 : 0,
                    0,
                    cutDirection == SwingDirection.VERTICAL ? angle : 0
                );
            Debug.Log(psPreExplode.transform.rotation.eulerAngles);
            useAI = false;
            myRigidbody.velocity = Vector3.zero;
            return new AttackFeedback(transform.position, AttackFeedbackReaction.CUTS_THRU, bounciness);
        }

        return null;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            UnityHelperUtils.CreateKnockback(
                myRigidbody,
                transform.position,
                player.position,
                attackLandedBounciness
            );
            attackTimeElapsed = 0;
            currentStage = SlimeStage.ATTACK_AFTERMATH;

            AttackFeedback af = other.gameObject.GetComponent<PlayerInput>()
                .PostAttack(
                    new AttackData(transform.position, SwingDirection.NONE, 1, attackLandedBounciness, false)
                );
        }
    }

    private void DieHideSelf()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        Destroy(GetComponent<Rigidbody>());
    }
}
