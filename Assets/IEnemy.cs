public interface IEnemy
{
    AttackFeedback PostAttack(AttackData attackData);
}