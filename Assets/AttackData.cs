using UnityEngine;

public enum SwingDirection { NONE, HORIZONTAL, VERTICAL }

public class AttackData
{
    public Vector3 myLocation;
    public SwingDirection slashDirection;
    public int weaponAttack;
    public float bounciness;
    public bool isMidair;

    public AttackData(
        Vector3 myLocation,
        SwingDirection slashDirection,
        int weaponAttack,
        float bounciness,
        bool isMidair)
    {
        this.myLocation = myLocation;
        this.slashDirection = slashDirection;
        this.weaponAttack = weaponAttack;
        this.bounciness = bounciness;
        this.isMidair = isMidair;
    }
}
