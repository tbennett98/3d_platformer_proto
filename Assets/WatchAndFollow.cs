﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchAndFollow : MonoBehaviour
{
    public Transform refTrans;
    private Vector3 refTransPrevPos;
    public float inputLookAroundSensitivity = 1;
    public float autoCamLookAroundSensitivity = 2;
    public bool autoCamera;
    public float autoCameraResetTime = 1;
    private float autoCameraResetCountDown;
    public float rotX, rotY;
    public float distance;

    void Start()
    {
        refTransPrevPos = refTrans.position;
    }

    void LateUpdate()
    {
        float deltaRotX = Input.GetAxis("Mouse X") * inputLookAroundSensitivity;

        if (deltaRotX == 0)
        {
            if (ProcessCountDown())
            {
                deltaRotX = CalculateAutoTurnRate() * autoCamLookAroundSensitivity;
            }
        }
        else
        {
            ResetCountDown();
        }

        rotX += deltaRotX * Time.deltaTime;
        UpdatePositionAndRotation();

        refTransPrevPos = refTrans.position;
    }

    private bool ProcessCountDown()
    {
        if (!autoCamera)
        {
            autoCameraResetCountDown += Time.deltaTime;
            if (autoCameraResetCountDown >= autoCameraResetTime)
            {
                autoCamera = true;
            }
        }

        return autoCamera;
    }

    private void ResetCountDown()
    {
        autoCamera = false;
        autoCameraResetCountDown = 0;
    }

    private float CalculateAutoTurnRate()
    {
        Vector3 mvtment = (refTrans.position - refTransPrevPos) / Time.deltaTime;
        Vector3 mvtmentWithRespectCam = Quaternion.Inverse(transform.rotation) * mvtment;
        return mvtmentWithRespectCam.x;
    }

    private void UpdatePositionAndRotation()
    {
        Vector3 distVector = new Vector3(0, 0, -distance);
		Quaternion rot = Quaternion.Euler(rotY, rotX, 0);
		transform.position = refTrans.position + (rot * distVector);
        transform.LookAt(refTrans);
    }
}
