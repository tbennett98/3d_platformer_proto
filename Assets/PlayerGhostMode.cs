﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGhostMode : MonoBehaviour
{
    public float upDownSpeed = 100;
    private PlayerInput myPlayerInput;

    void Start()
    {
        myPlayerInput = GetComponent<PlayerInput>();
    }

    void Update()
    {
        if (Input.GetButtonDown("ToggleCheatMode"))
        {
            // DISABLE ME AND ENABLE REGULAR PLAYER MODE
            GetComponent<MeshCollider>().enabled = true;
            GetComponent<Rigidbody>().isKinematic = false;
            myPlayerInput.enabled = true;
            this.enabled = false;
        }

        // INPUT
        float garb;
        Vector3 deltaMvt =
            myPlayerInput.JustYRotation(myPlayerInput.refCamera.transform.rotation)
                * myPlayerInput.GetInputMovement(out garb)
                * myPlayerInput.speed
                + GetGhostVerticalMovement();
        transform.Translate(deltaMvt * Time.deltaTime);
    }

    private Vector3 GetGhostVerticalMovement()
    {
        if (Input.GetButton("Jump"))
        {
            return Vector3.up * upDownSpeed;
        }
        if (Input.GetButton("Attack"))
        {
            return Vector3.down * upDownSpeed;
        }
        return Vector3.zero;
    }
}
