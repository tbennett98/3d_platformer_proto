﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyObject : MonoBehaviour, IEnemy
{
    public Transform bounceDirection;
    public float bounciness;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        if (bounceDirection != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, bounceDirection.position);
        }
    }

    public AttackFeedback PostAttack(AttackData attackData)
    {
        Vector3 deltaLoc = transform.position - bounceDirection.position;
        deltaLoc = attackData.myLocation + deltaLoc;

        return new AttackFeedback(deltaLoc, AttackFeedbackReaction.CUTS_THRU, bounciness);
    }
}
